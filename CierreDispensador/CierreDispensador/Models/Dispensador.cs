﻿using System;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace CierreDispensador.Models
{
    public class Dispensador
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        [ForeignKey(typeof(Combustible))]
        public int IDCombustible { get; set; }

        [OneToOne("IDCombustible")]
        public Combustible Producto { get; set; }
        public double MecanicoInicial { get; set; }
        public double MecanicoFinal { get; set; }
        public double LitrosVendidos { get; set; }
        public double Precio { get; set; }
    }
}
