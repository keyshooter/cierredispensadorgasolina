﻿using System;
using SQLite;

namespace CierreDispensador.Models
{
    public class Combustible
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        [Unique]
        public string Nombre { get; set; }
        public double PrecioXLitro { get; set; }
    }
}
