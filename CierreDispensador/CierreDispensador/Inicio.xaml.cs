﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CierreDispensador.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CierreDispensador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inicio : TabbedPage
    {
        public Inicio()
        {
            InitializeComponent();

            Title = "Cierre de dispensador";
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            listaCombustibles.ItemsSource = await App.Database.GetCombustiblesAsync();

            List<Dispensador> dispensadores = await ConectarCombustibles(await App.Database.GetDispensadoresAsync());
            CalcularVentas(dispensadores);
            listaDispensadores.ItemsSource = dispensadores;
        }

        private async Task<List<Dispensador>> ConectarCombustibles(List<Dispensador> dispensadores)
        {
            List<Dispensador> dispensadoresConectados = new List<Dispensador>();

            foreach(Dispensador dispensador in dispensadores)
            {
                dispensador.Producto = await App.Database.GetCombustibleAsync(dispensador.IDCombustible);
                dispensadoresConectados.Add(dispensador);
            }

            return dispensadoresConectados;
        }

        private void CalcularVentas(List<Dispensador> dispensadores)
        {
            double totalVentas = 0;

            foreach (Dispensador dispensador in dispensadores)
            {
                dispensador.Precio = dispensador.LitrosVendidos * dispensador.Producto.PrecioXLitro;
                totalVentas += dispensador.Precio;
            }

            LblTotalVentas.FormattedText = ("Total: " + totalVentas.ToString("C"));
        }

        async void OnAddCombustible(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PaginaCombustible
            {
                BindingContext = new Combustible()
            });
        }

        async void OnGasolinaSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new PaginaCombustible
                {
                    BindingContext = e.SelectedItem as Combustible
                });
            }
        }

        async void OnAddDispensador(object sender, EventArgs e)
        {
            if (((List<Combustible>)listaCombustibles.ItemsSource).Count < 1)
            {
                await DisplayAlert("Sin combustibles", "No hay combustibles agregados\nAñada combustibles en la página de combustibles", "Entendido");
                return;
            }

            await Navigation.PushAsync(new PaginaDispensador
            {
                BindingContext = new Dispensador()
            });
        }

        async void OnDispensadorSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new PaginaDispensador
                {
                    BindingContext = e.SelectedItem as Dispensador
                });
            }
        }
    }
}