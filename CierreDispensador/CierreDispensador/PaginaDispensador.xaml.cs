﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CierreDispensador.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CierreDispensador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaDispensador : ContentPage
    {
        public PaginaDispensador()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            listaCombustibles.ItemsSource = await App.Database.GetCombustiblesAsync();

            bool found = false;

            if (((Dispensador)BindingContext).Producto != null)
            {
                for(int i = 0; i < listaCombustibles.ItemsSource.Count && !found; i++)
                {
                    if (((Combustible)(listaCombustibles.ItemsSource[i])).ID == ((Dispensador)BindingContext).IDCombustible)
                    {
                        listaCombustibles.SelectedIndex = i;
                        found = true;
                    }
                }
            }
        }

        async void OnSaveClick(object sender, EventArgs e)
        {
            if (Double.Parse(mecanicoInicial.Text) > 0 && Double.Parse(mecanicoFinal.Text) > 0 && listaCombustibles.SelectedItem != null)
            {
                Dispensador dispensador = (Dispensador)BindingContext;
                dispensador.MecanicoInicial = Double.Parse(mecanicoInicial.Text);
                dispensador.MecanicoFinal = Double.Parse(mecanicoFinal.Text);
                dispensador.Producto = (Combustible)listaCombustibles.SelectedItem;
                DeterminarDispensador(dispensador);
                await App.Database.SaveDispensadorAsync(dispensador);
                await Navigation.PopAsync();
            }
            else
            {
                await DisplayAlert("Datos erroneos", "Verifique que los trompos sean números positivos y que el combustible esté selecionado", "Entendido");
            }
        }

        private void DeterminarDispensador(Dispensador dispensador)
        {
            dispensador.LitrosVendidos = Math.Abs(dispensador.MecanicoFinal - dispensador.MecanicoInicial);
            dispensador.IDCombustible = dispensador.Producto.ID;
            dispensador.Precio = dispensador.LitrosVendidos * dispensador.Producto.PrecioXLitro;
        }

        async void OnDeleteClick(object sender, EventArgs e)
        {
            Dispensador dispensador = (Dispensador)BindingContext;
            await App.Database.DeleteDispensadorAsync(dispensador);
            await Navigation.PopAsync();
        }
    }
}