﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CierreDispensador.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CierreDispensador
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaginaCombustible : ContentPage
    {
        public PaginaCombustible()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext != null)
            {
                Title = (((Combustible)BindingContext).ID != 0) ? "Editar combustible" : "Agregar combustible" ;
            }
        }

        async void OnSaveClick(object sender, EventArgs e)
        {
            if (Double.Parse(precio.Text) > 0)
            {

                try
                {
                    Combustible combustible = (Combustible)BindingContext;
                    combustible.Nombre = nombre.Text;
                    combustible.PrecioXLitro = Double.Parse(precio.Text);
                    await App.Database.SaveCombustibleAsync(combustible);
                    await Navigation.PopAsync();
                }
                catch(Exception)
                {
                    await DisplayAlert("Nombre de combustible repetido", "El nombre del combustible que trata de ingresar ya existe", "Entendido");
                }
            }
            else
            {
                await DisplayAlert("Precio en negativos", "El precio de un combustible no puede ser negativo", "Entendido");
            }
        }

        async void OnDeleteClick(object sender, EventArgs e)
        {
            Combustible combustible = (Combustible)BindingContext;
            await App.Database.DeleteCombustibleAsync(combustible);
            await Navigation.PopAsync();
        }
    }
}