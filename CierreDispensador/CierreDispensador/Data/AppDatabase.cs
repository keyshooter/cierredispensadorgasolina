﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using CierreDispensador.Models;
using System;

namespace CierreDispensador.Data
{
    public class AppDatabase
    {
        readonly SQLiteAsyncConnection _database;

        public AppDatabase(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Combustible>().Wait();
            _database.CreateTableAsync<Dispensador>().Wait();
        }

        public Task<List<Combustible>> GetCombustiblesAsync()
        {
            return _database.Table<Combustible>().ToListAsync();
        }

        public Task<Combustible> GetCombustibleAsync(int id)
        {
            return _database.Table<Combustible>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        public Task<int> SaveCombustibleAsync(Combustible combustible)
        {
            try
            {
                if (combustible.ID != 0)
                    return _database.UpdateAsync(combustible);
                else
                    return _database.InsertAsync(combustible);
            }
            catch(SQLiteException e)
            {
                throw new Exception("Nombre repetido, ingrese un nuevo nombre", e);
            }
        }

        public Task<int> DeleteCombustibleAsync(Combustible combustible)
        {
            return _database.DeleteAsync(combustible);
        }

        public Task<List<Dispensador>> GetDispensadoresAsync()
        {
            return _database.Table<Dispensador>().ToListAsync();
        }

        public Task<Dispensador> GetDispensadorAsync(int id)
        {
            return _database.Table<Dispensador>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        public Task<int> SaveDispensadorAsync(Dispensador dispensador)
        {
            if (dispensador.ID != 0)
                return _database.UpdateAsync(dispensador);
            else
                return _database.InsertAsync(dispensador);
        }

        public Task<int> DeleteDispensadorAsync(Dispensador dispensador)
        {
            return _database.DeleteAsync(dispensador);
        }
    }
}
